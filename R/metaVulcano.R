#'Vulcano plot for meta-analysis results with metafor
#'
#'\code{metaVulcano}
#'
#'@param res.mat results res.matrix with LOR or SD values for all studies. Rownames must
#'be GO IDs, and a column named threshold must be added, containing a coloring
#'factor for significant up, down and non-significant GO IDs.
#'@param alpha threshold value for p.adjust value
#'@param upcol string specifying the color for the significant up GO IDs
#'@param noncol string specifying the color for the non significant GO IDs
#'@param downcol string specifying the color for the significant down GO IDs
#'@param xlab string specifying the x title, by default "log2 Odds Ratio"
#'@param ylab string specifying the y title, by default "-log10 FDR"
#'@param titl string specifying the plot title, by default "Volcano plot"
#'@import ggplot2
#'@export
#'
metaVulcano <- function (res.mat, alpha = 0.05, upcol = "royalblue", noncol = "white",
                         downcol = "firebrick2", xlab = "log2 Odds Ratio",
                         ylab = "-log10 FDR", titl = "Volcano plot") {
  res.mat$threshold <- rep(noncol, nrow(res.mat))
  sig.up <- (res.mat[, "p.adjust"] < alpha) & (res.mat[, "summary_LOR"] > 0)
  sig.down <- (res.mat[, "p.adjust"] < alpha) & (res.mat[, "summary_LOR"] < 0)
  res.mat$threshold[sig.up] <- upcol
  res.mat$threshold[sig.down] <- downcol
  res.mat$threshold <- as.factor(res.mat$threshold)

  p <- ggplot2::ggplot(res.mat, ggplot2::aes(x = res.mat$summary_LOR,
                                             y = -log(res.mat[,"p.adjust"], base = 10),
                                             colour = res.mat$threshold))
  p <- p + ggplot2::geom_point(alpha = 0.8, size = 4, colour = res.mat$threshold)
  p <- p + ggplot2::xlim(c(-2, 2)) + ggplot2::ylim(c(0, 3.1))
  p <- p + ggplot2::xlab(xlab)
  p <- p + ggplot2::ylab(ylab)
  p <- p + ggplot2::labs(title = titl)
  p <- p + ggplot2::theme(legend.position = "none")
  # #to change size of title in axis x
  # p <- p + ggplot2::theme(
  #   axis.title.x= ggplot2::element_text(colour= "black", size = 35 ))
  # #to change size of title in axis y
  # p <- p + ggplot2::theme(
  #   axis.title.y= ggplot2::element_text(colour= "black", size = 35 ))
  # #to change size of numbers
  # p <- p + ggplot2::theme(
  #   axis.text.x = ggplot2::element_text(colour= "black", size = 35 ))
  # #to change size of numbers
  # p <- p + ggplot2::theme(
  #   axis.text.y = ggplot2::element_text(colour= "black", size = 35 ))
  return(p)

}
