#' Apply Hipathia structure to limma results
#'
#'\code{limmaToHipathia} Changes data format from limma fit object to a data
#'frame compatible with hipathia package
#'
#'
#'@param fit MArrayLM limma object
#'@param contrastNumber number of the contrast in fit object to convert
#'@param pathways pathways hipathia object
#'@return data frame with pathways ID as rownames, pathway name, UP/DOWN sign,
#'t statistic, p value and adjusted p value.
#'@import hipathia
#'@export

limmaToHipathia <- function(fit, contrastNumber, pathways){
  pathIDs <- rownames(fit$p.value)
  statistic <- fit$t[,contrastNumber]
  updown <- ifelse(statistic > 0, "UP", "DOWN")
  pVal <- fit$p.value[,contrastNumber]
  FDRpVal <- fit$p.adj[,contrastNumber]
  SE <- sqrt(fit$s2.post) * fit$stdev.unscaled
  name <- hipathia::get_path_names(pathways, pathIDs)

  hipathiaDataFrame <- data.frame(name, updown, statistic, pVal, FDRpVal, SE, stringsAsFactors = FALSE)
  rownames(hipathiaDataFrame) <- pathIDs
  colnames(hipathiaDataFrame) <- c("name", "UP/DOWN", "statistic", "p.value",
                                   "FDRp.value", "SE")

  return(hipathiaDataFrame)
}
