#' Perform Hipathia signal computing
#'
#'\code{doHipathia} Performs hipathia signal computing
#'
#'
#'@param gse Expressionset object
#'@param pathways pathways hipathia object
#'@param decompose Boolean, whether to compute the values for the decomposed
#'subpathways. By default, effector subpathways are not computed.
#'@param verbose Boolean, whether to show details about the results of the
#'execution of hipathia. By default, details are not shown.
#'@return Hipathia results object
#'@import hipathia
#'@import SummarizedExperiment
#'@export
#'
doHipathia <- function(gse, pathways, decompose = FALSE, verbose = FALSE)
  {
  # Data scaling & normalization -----------------------------------------------
  sum <- SummarizedExperiment::makeSummarizedExperimentFromExpressionSet(gse)
  exp_data <- hipathia::normalize_data(sum)
  # Signal computing -----------------------------------------------------------
  results <- hipathia::hipathia(
    exp_data, pathways, decompose = decompose, verbose = verbose
    )
  return(results)
}
