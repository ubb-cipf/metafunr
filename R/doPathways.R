#' Pathway/function activation analysis
#'
#'\code{doPathways} Performs Hipathia pathway/function activation analysis
#'
#'
#'@param results results Hipathia object
#'@param pathways pathways hipathia object
#'@param contrast vector of strings with the contrasts to perform.
#'@param pathOrFunc string denoting which activation analysis is going to be
#'performed. Values accepted are c("pathways", "uniprot", "GO").
#'@param batch boolean, wherther there's available data about batch or not
#'@param paired boolean, wherther there's available data about paired samples or
#'not.
#'@return list with two lists: pathway results matrix and pathways results
#'summary for each contrast
#'@import hipathia
#'@import SummarizedExperiment
#'@import stats
#'@importFrom methods new
#'@export
#'
doPathways <- function(results, pathways, contrast, pathOrFunc = "pathways",
                       batch = FALSE, paired = FALSE)
{
  if (pathOrFunc == "pathways") {
    path_vals <- hipathia::get_paths_data(results)
  } else if(pathOrFunc == "uniprot" | pathOrFunc == "GO"){
    path_vals <- hipathia::quantify_terms(results, pathways, dbannot = pathOrFunc)
  } else {
    stop("pathOrFunc should be 'pathways', 'uniprot' or 'GO'")
  }

  # Pathway/function activation analysis ---------------------------------------
  variable <- path_vals@colData$Condition

  if (paired == TRUE){
    if (batch == TRUE){
      pair <- path_vals@colData$Pair
      batch <- path_vals@colData$Smoking
      metaDt <- data.frame(labelDescription = c("Pair" ,"Condition", "Smoking"),
                           row.names = c("Pair" ,"Condition", "Smoking"))
      fenodata <- methods::new("AnnotatedDataFrame", data = data.frame(pair, variable, batch), varMetadata = metaDt)
      rownames(fenodata) <- colnames(assay(path_vals))
      data <- methods::new("ExpressionSet", exprs = assay(path_vals), phenoData = fenodata)
    } else {
      pair <- path_vals@colData$Pair
      metaDt <- data.frame(labelDescription = c("Pair" ,"Condition"),
                           row.names = c("Pair" ,"Condition"))
      fenodata <- methods::new("AnnotatedDataFrame", data = data.frame(pair, variable), varMetadata = metaDt)
      rownames(fenodata) <- colnames(assay(path_vals))
      data <- methods::new("ExpressionSet", exprs = assay(path_vals), phenoData = fenodata)
    }
  } else if(batch == TRUE){
    batch <- path_vals@colData$Smoking
    metaDt <- data.frame(labelDescription = c("Condition", "Smoking"),
                         row.names = c("Condition", "Smoking"))
    fenodata <- methods::new("AnnotatedDataFrame", data = data.frame(variable, batch), varMetadata = metaDt)
    rownames(fenodata) <- colnames(assay(path_vals))
    data <- methods::new("ExpressionSet", exprs = assay(path_vals), phenoData = fenodata)
  } else{
    metaDt <- data.frame(labelDescription = c("Condition"),
                         row.names = c("Condition"))
    fenodata <- methods::new("AnnotatedDataFrame", data = data.frame(variable), varMetadata = metaDt)
    rownames(fenodata) <- colnames(assay(path_vals))
    data <- methods::new("ExpressionSet", exprs = assay(path_vals), phenoData = fenodata)
  }


  fit <- diffExp(data, variable, contrast, batch = FALSE, paired = FALSE)
  fit$p.adj <- apply (fit$p.value, 2, stats::p.adjust, method = "BH")


  return(fit)
}
