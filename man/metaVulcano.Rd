% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/metaVulcano.R
\name{metaVulcano}
\alias{metaVulcano}
\title{Vulcano plot for meta-analysis results with metafor}
\usage{
metaVulcano(res.mat, alpha = 0.05, upcol = "royalblue",
  noncol = "white", downcol = "firebrick2", xlab = "log2 Odds Ratio",
  ylab = "-log10 FDR", titl = "Volcano plot")
}
\arguments{
\item{res.mat}{results res.matrix with LOR or SD values for all studies. Rownames must
be GO IDs, and a column named threshold must be added, containing a coloring
factor for significant up, down and non-significant GO IDs.}

\item{alpha}{threshold value for p.adjust value}

\item{upcol}{string specifying the color for the significant up GO IDs}

\item{noncol}{string specifying the color for the non significant GO IDs}

\item{downcol}{string specifying the color for the significant down GO IDs}

\item{xlab}{string specifying the x title, by default "log2 Odds Ratio"}

\item{ylab}{string specifying the y title, by default "-log10 FDR"}

\item{titl}{string specifying the plot title, by default "Volcano plot"}
}
\description{
\code{metaVulcano}
}
