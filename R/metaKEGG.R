#' KEGG meta-analysis
#'
#'\code{metaKEGG} computes differential expression analysis for a given data-set.
#'Saves in a given path dataframes containing the results for each method,
#'the summary of all methods, the intersection of significant results and the
#'union of significant results.
#'
#'
#'@param mat.lor matrix with LOR values for all studies. Rownames must be KEGG IDs
#'and it must have one column by file or study.
#'@param mat.sd matrix with SD values for all studies. Rownames must be KEGG IDs
#'and it must have one column by file or study.
#'@param methods vector with one or several of the following meta-analysis
#'methods: c("DL", "HE", "HS", "FE")
#'@param res.path path were the results should be stored. By default, the
#'current working directory.
#'@param adj.method correction method, c("holm", "hochberg", "hommel",
#'"bonferroni", "BH", "BY", "fdr"). By default, "fdr".
#'@param alpha threshold to detect significant results. By default, 0.05.
#'@param OR.threshold threshold to detect OR. By default, 0.5.
#'@return dataframe with results for each method
#'@import metafor
#'@import mdgsa
#'@import stats
#'@import utils
#'@export

metaKEGG <- function(mat.lor, mat.sd, methods, res.path = getwd(), adj.method = "fdr",
                   alpha = 0.05, OR.threshold = 0.5) {
  res <- matrix (NA, nrow = length (methods), ncol = 6)
  colnames(res) <- c("over", "under", "sig.over", "sig.under", "sig.or.over", "sig.or.under")
  rownames(res) <- methods

  for (i in methods){
    meta_analysis <- lapply(1:length(rownames(mat.lor)),
                            function(x){yi = metafor::rma(mat.lor[x, ],
                                                          sei =mat.sd[x, ],
                                                          method = i)})
    names(meta_analysis) <- rownames(mat.lor)
    result_meta <- as.data.frame(
      do.call(
        "rbind",
        lapply(meta_analysis,
               function(x){
                 c(x$ci.lb, x$b, x$ci.ub, x$pval, x$QE, x$QEp, x$se, x$tau2,
                   x$I2, x$H2)
               })))
    colnames(result_meta) <- c("lower_bound", "summary_LOR", "upper_bound",
                               "pvalue", "QE", "QEp", "SE", "tau2", "I2", "H2")
    p.adjust <- stats::p.adjust(result_meta[,4], method = adj.method)
    result_meta <- round(cbind(result_meta, p.adjust), 3)

    if (file.exists(res.path)){
      setwd(res.path)
    } else {
      dir.create(res.path, recursive = TRUE)
      setwd(res.path)
    }

    res[i, "over"]      <-  sum(result_meta[, "summary_LOR"] > 0)
    res[i, "under"]    <-  sum(result_meta[, "summary_LOR"] < 0)
    res[i,"sig.over"]   <-  sum(result_meta[, "summary_LOR"] > 0 &
                                  result_meta[,"p.adjust"] < alpha)
    res[i,"sig.under"] <-  sum(result_meta[, "summary_LOR"] < 0 &
                                 result_meta[,"p.adjust"] < alpha)
    res[i,"sig.or.over"]   <-  sum(result_meta[, "summary_LOR"] >  OR.threshold &
                                     result_meta[,"p.adjust"] < alpha)
    res[i,"sig.or.under"] <-  sum(result_meta[, "summary_LOR"] < -OR.threshold &
                                    result_meta[,"p.adjust"] < alpha)

    # Getting common IDs for all models
    if (i == "DL"){
      DL <- result_meta
      DL[,"name"] <- mdgsa::getKEGGnames(rownames(DL))
      DL[, "ID"]   <- rownames(DL)
      DL <- DL[c("ID", "name", "lower_bound", "summary_LOR", "upper_bound", "pvalue",
                 "p.adjust", "QE", "QEp", "SE", "tau2", "I2", "H2" )]
      write.table(DL, "all.results.DL.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      DLs = subset(DL, DL[,"p.adjust"] < alpha)
      write.table(DLs, "sig.results.DL.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      idDL = rownames(DLs)
    } else if (i == "HE"){
      HE <- result_meta
      HE[,"name"] <- mdgsa::getKEGGnames(rownames(HE))
      HE[, "ID"]   <- rownames(HE)
      HE <- HE[c("ID", "name", "lower_bound", "summary_LOR", "upper_bound", "pvalue",
                 "p.adjust","QE", "QEp", "SE", "tau2", "I2", "H2" )]
      write.table(HE, "all.results.HE.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      HEs = subset(HE, HE[,"p.adjust"] < alpha)
      write.table(HEs, "sig.results.HE.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      idHE = rownames(HEs)
    } else if (i == "HS"){
      HS <- result_meta
      HS[,"name"] <- mdgsa::getKEGGnames(rownames(HS))
      HS[, "ID"]   <- rownames(HS)
      HS <- HS[c("ID", "name", "lower_bound", "summary_LOR", "upper_bound", "pvalue",
                 "p.adjust","QE", "QEp", "SE", "tau2", "I2", "H2" )]
      write.table(HS, "all.results.HS.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      HSs = subset(HS, HS[,"p.adjust"] < alpha)
      write.table(HSs, "sig.results.HS.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      idHS = rownames(HSs)
    } else if (i == "FE"){
      FE <- result_meta
      FE[,"name"] <- mdgsa::getKEGGnames(rownames(FE))
      FE[, "ID"]   <- rownames(FE)
      FE <- FE[c("ID", "name", "lower_bound", "summary_LOR", "upper_bound", "pvalue",
                 "p.adjust","QE", "QEp", "SE", "tau2", "I2", "H2" )]
      write.table(FE, "all.results.FE.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      FEs = subset(FE, FE[,"p.adjust"] < alpha)
      write.table(FEs, "sig.results.FE.txt", sep = "\t",
                  quote = FALSE, row.names = FALSE)
      idFE = rownames(FEs)
    }
  }

  # Global results
  write.table (res, file = "all.results.all.methods.txt",
               append = TRUE, quote = FALSE, sep = "\t",
               row.names = TRUE, col.names = TRUE)

  if(length(methods) > 1){
    ids <- c()
    for (m in methods) {
      ids <- c(ids, paste0(id, m))
    }
    # Intersection
    intersectmodels =  Reduce(intersect,  lapply(ids, function(x) get(x)))
    write.table (intersectmodels, file = "sig_res_intersection.txt",
                 quote = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)
    # Union
    unionmodels = Reduce(c, lapply(ids, function(x) get(x)))
    mat.union <- as.data.frame(table(unionmodels))
    mat.union <- mat.union[order(mat.union$Freq, decreasing = TRUE),]
    write.table (mat.union, file = "res_union.txt",append = TRUE,
                 quote = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)
  }

  return(meta_analysis)
}
