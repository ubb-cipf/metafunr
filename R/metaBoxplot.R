#'Boxplots for meta-analysis matrices
#'
#'\code{metaBoxplot}
#'
#'@param mat matrix with LOR or SD values for all studies. Rownames must be GO
#'IDs and it must have one column by file or study.
#'@param value string specifying wherther the matrix includes sd or lor values,
#'c("sd", "lor")
#'@param ylab string specifying the y title, normally "Log Odds Ratio" or
#'"Standard Error"
#'@param xlab string specifying the x title, by default "Studies".
#'@import ggplot2
#'@import reshape
#'@export
#'
metaBoxplot <- function (mat, value, ylab,  xlab = "Studies") {
  gmat <- mat
  colnames(gmat) <- toupper(colnames(mat))
  g2mat <- reshape::melt(gmat)
  colnames(g2mat) <- c("function", "study", value)

  p <- ggplot2::ggplot(g2mat, ggplot2::aes(study, g2mat[, value]))
  p <- p + ggplot2::geom_boxplot()
  p <- p + ggplot2::xlab(xlab)
  p <- p + ggplot2::ylab(ylab)
  # #to change size of title in axis x
  # p <- p + ggplot2::theme(
  #   axis.title.x= ggplot2::element_text(colour= "black", size = 40 ))
  # #to change size of title in axis y
  # p <- p + ggplot2::theme(
  #   axis.title.y= ggplot2::element_text(colour= "black", size = 40 ))
  # #to change size of numbers
  # p <- p + ggplot2::theme(
  #   axis.text.x = ggplot2::element_text(colour= "black", size = 20 ))
  # #to change size of numbers
  # p <- p + ggplot2::theme(
  #   axis.text.y = ggplot2::element_text(colour= "black", size = 30 ))
  return(p)
}
