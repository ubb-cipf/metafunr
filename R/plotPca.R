#' PCA plot
#'
#'\code{plotPca} plots a PCA dataframe
#'
#'@param pcaDf data.frame containing PCA
#'@param condition coloring variable
#'@param palette color palette. If missing, a color blind palette is used
#'@param addNames boolean, adds sample names to the plot or not, TRUE by default
#'@param PC1 First principal component to plot, 1 by default
#'@param PC2 Second principal component to plot, 2 by default
#'@param hjust ggplot2 hjust parameter. Affects the display of labels in case
#'that addNames is set as TRUE
#'@param vjust ggplot2 vjust parameter. Affects the display of labels in case
#'that addNames is set as TRUE
#'@import ggplot2
#'@export
#'
plotPca <- function (pcaDf, condition, palette = "default", addNames = TRUE, PC1 = 1,
                     PC2 = 2, hjust = 0.5, vjust = 0.5) {
  if (palette == "default") {
    palette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
  }
  g =ggplot(pcaDf, aes (x = pcaDf[,PC1], y = pcaDf[,PC2], color = condition, label = row.names(pcaDf))) +
    geom_point() +
    xlab(paste("PC", PC1, ": ", round(pcaDf$var.exp[PC1,1] * 100), "% explained variance", sep = "")) +
    ylab(paste("PC", PC2, ": ", round(pcaDf$var.exp[PC2,1] * 100), "% explained variance", sep = "")) +
    ggtitle("PCA plot") +
    theme(plot.title = element_text(hjust = 0.5)) +
    scale_colour_manual(values=palette) +
    scale_fill_manual(values=palette)
  if (addNames == TRUE) {
    g = g + geom_text(size=3, hjust = hjust, vjust =vjust)
  }
  return(g)
}
